package com.epam.extlab2018.finalProject.option13.dto;

import java.math.BigDecimal;

public class Bill {
    private long billId;
    private BigDecimal score;
    private boolean isPaid;

    public Bill() {
    }

    public long getBillId() {
        return billId;
    }

    public void setBillId(long billId) {
        this.billId = billId;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }


    @Override
    public String toString() {
        return "Bill{" +
                "billId=" + billId +
                ", score=" + score +
                ", isPaid=" + isPaid +
                '}';
    }

}
