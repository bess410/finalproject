package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;
import com.epam.extlab2018.finalProject.option13.util.mappers.HotelRoomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.List;

@Repository
public class HotelRoomRepositoryImpl implements HotelRoomRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public long createHotelRoom(int seatNumber, ApartmentClass apartmentClass, BigDecimal dailyCost) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("seatNumber", seatNumber)
                .addValue("apartmentClass", apartmentClass.toString())
                .addValue("dailyCost", dailyCost);
        namedParameterJdbcTemplate.update("Insert into hotelRoom (seatNumber, apartmentClass, dailyCost) Values" +
                "(:seatNumber, :apartmentClass, :dailyCost)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void updateHotelRoom(long hotelRoomId, int seatNumber, ApartmentClass apartmentClass, BigDecimal dailyCost) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update hotelRoom Set seatNumber = ?, apartmentClass = ?, dailyCost = ? Where ID = ?",
                seatNumber, apartmentClass.toString(), dailyCost, hotelRoomId);
    }

    @Override
    public void deleteHotelRoom(long hotelRoomId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete From hotelRoom Where id = ?", hotelRoomId);
    }

    @Override
    public HotelRoom findHotelRoomById(long hotelRoomId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From hotelRoom Where id = ?", new Object[]{hotelRoomId}, new HotelRoomMapper());
    }

    @Override
    public List<HotelRoom> getAllHotelRooms() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From hotelRoom", new HotelRoomMapper());
    }
}
