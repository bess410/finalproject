package com.epam.extlab2018.finalProject.option13.dto;

import java.time.LocalDate;

public class Request {
    private long requestId;
    private int seatNumber;
    private ApartmentClass apartmentClass;
    private LocalDate start;
    private LocalDate end;
    private long userId;
    private long hotelRoomId;
    private long billId;

    public Request() {
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public ApartmentClass getApartmentClass() {
        return apartmentClass;
    }

    public void setApartmentClass(ApartmentClass apartmentClass) {
        this.apartmentClass = apartmentClass;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getHotelRoomId() {
        return hotelRoomId;
    }

    public void setHotelRoomId(long hotelRoomId) {
        this.hotelRoomId = hotelRoomId;
    }

    public long getBillId() {
        return billId;
    }

    public void setBillId(long billId) {
        this.billId = billId;
    }


    @Override
    public String toString() {
        return "Request{" +
                "requestId=" + requestId +
                ", seatNumber=" + seatNumber +
                ", apartmentClass=" + apartmentClass +
                ", start=" + start +
                ", end=" + end +
                ", userId=" + userId +
                ", hotelRoomId=" + hotelRoomId +
                ", billId=" + billId +
                '}';
    }
}
