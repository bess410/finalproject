package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.Bill;

import java.math.BigDecimal;
import java.util.List;

public interface BillRepository {
    long createBill(BigDecimal score, boolean isPaid);

    void updateBill(long billId, BigDecimal score, boolean isPaid);

    void deleteBill(long billId);

    Bill findBillById(long billId);

    List<Bill> getAllBills();
}
