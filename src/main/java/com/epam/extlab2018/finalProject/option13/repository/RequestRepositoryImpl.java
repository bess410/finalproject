package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.Request;
import com.epam.extlab2018.finalProject.option13.util.mappers.RequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;

@Repository
public class RequestRepositoryImpl implements RequestRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public long createRequest(int seatNumber, ApartmentClass apartmentClass, LocalDate start, LocalDate end, long user_id) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("seatNumber", seatNumber)
                .addValue("apartmentClass", apartmentClass.toString())
                .addValue("start", start)
                .addValue("end", end)
                .addValue("user_id", user_id);
        namedParameterJdbcTemplate.update("Insert into request (seatNumber, apartmentClass, start, end, user_id) Values" +
                "(:seatNumber, :apartmentClass, :start, :end, :user_id)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void updateRequest(long requestId, int seatNumber, ApartmentClass apartmentClass, LocalDate start, LocalDate end, long user_id, long hotelRoom_id, long bill_id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update request Set seatNumber = ?, apartmentClass = ?, start = ?, end = ?, user_id = ?, hotelRoom_id = ?, bill_id = ? Where ID = ?",
                seatNumber, apartmentClass.toString(), start, end, user_id, hotelRoom_id, bill_id, requestId);
    }

    @Override
    public void updateRequest(long requestId, int seatNumber, ApartmentClass apartmentClass, LocalDate start, LocalDate end) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update request Set seatNumber = ?, apartmentClass = ?, start = ?, end = ? Where ID = ?",
                seatNumber, apartmentClass.toString(), start, end, requestId);
    }

    @Override
    public void deleteRequest(long requestId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete From request Where id = ?", requestId);
    }

    @Override
    public Request findRequestById(long requestId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From request Where id = ?", new Object[]{requestId}, new RequestMapper());
    }

    @Override
    public List<Request> getAllRequests() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From request", new RequestMapper());
    }
}
