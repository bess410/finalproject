package com.epam.extlab2018.finalProject.option13.util.mappers;

import com.epam.extlab2018.finalProject.option13.dto.Role;
import com.epam.extlab2018.finalProject.option13.dto.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNums) throws SQLException {
        User user = new User();
        user.setUserId(rs.getLong("id"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setLogin(rs.getString("login"));
        user.setPass(rs.getString("pass"));
        user.setRole(Role.valueOf(rs.getString("role").toUpperCase()));
        return user;
    }
}
