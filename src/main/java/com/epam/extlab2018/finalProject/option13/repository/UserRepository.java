package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.Role;
import com.epam.extlab2018.finalProject.option13.dto.User;

import java.util.List;

public interface UserRepository {
    long createUser(String name, String surname, String login, String pass, Role role);

    void updateUser(long userId, String name, String surname, String login, String pass, Role role);

    void deleteUser(long userId);

    User findUserById(long userId);

    List<User> getAllUsers();

    List<User> getAllClients();

    List<User> getAllAdmins();
}
