package com.epam.extlab2018.finalProject.option13;

import com.epam.extlab2018.finalProject.option13.dto.Role;
import com.epam.extlab2018.finalProject.option13.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Option13Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(Option13Application.class, args);
        UserRepository userRepository = context.getBean(UserRepository.class);
        System.out.println("Получаем список всех пользователей");
        userRepository.getAllUsers().forEach(System.out::println);
        System.out.println("Создаем нового пользователя");
        System.out.println();
        long id = userRepository.createUser("Андрей", "Стерхов", "bess410", "bess410", Role.CLIENT);
        System.out.println("Получаем список всех пользователей");
        userRepository.getAllUsers().forEach(System.out::println);
        System.out.println();
        System.out.println("Изменяем имя и фамилию пользователя с id = 15");
        userRepository.updateUser(15, "Гиви", "Карабидзе", "vanya666", "vanya666", Role.CLIENT);
        System.out.println("Получаем список всех пользователей");
        userRepository.getAllUsers().forEach(System.out::println);
        System.out.println();
        System.out.println("Удаляем пользователя с id = " + id);
        userRepository.deleteUser(id);
        System.out.println("Получаем список всех пользователей");
        userRepository.getAllUsers().forEach(System.out::println);
        System.out.println();
        System.out.println("Находим пользователя с id = 18");
        System.out.println(userRepository.findUserById(18));
    }
}
