package com.epam.extlab2018.finalProject.option13.dto;

public enum ApartmentClass {
    ECONOMIC, BUSINESS, LUX
}
