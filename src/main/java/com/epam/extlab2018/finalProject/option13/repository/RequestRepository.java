package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.Request;

import java.time.LocalDate;
import java.util.List;

public interface RequestRepository {
    long createRequest(int seatNumber, ApartmentClass apartmentClass, LocalDate start, LocalDate end, long userId);

    void updateRequest(long requestId, int seatNumber, ApartmentClass apartmentClass, LocalDate start, LocalDate end,
                       long userId, long hotelRoomId, long billId);

    void updateRequest(long requestId, int seatNumber, ApartmentClass apartmentClass, LocalDate start, LocalDate end);

    void deleteRequest(long requestId);

    Request findRequestById(long requestId);

    List<Request> getAllRequests();
}
