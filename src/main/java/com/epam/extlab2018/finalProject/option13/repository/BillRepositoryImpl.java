package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.Bill;
import com.epam.extlab2018.finalProject.option13.util.mappers.BillMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.List;

@Repository
public class BillRepositoryImpl implements BillRepository{

    @Autowired
    private DataSource dataSource;

    @Override
    public long createBill(BigDecimal score, boolean isPaid) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("score", score)
                .addValue("isPaid", isPaid);
        namedParameterJdbcTemplate.update("Insert into bill (score, isPaid) Values" +
                "(:score, :isPaid)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void updateBill(long billId, BigDecimal score, boolean isPaid) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update bill Set score = ?, isPaid = ? Where ID = ?",
                score, isPaid, billId);
    }

    @Override
    public void deleteBill(long billId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete from bill Where id = ?", billId);
    }

    @Override
    public Bill findBillById(long billId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From bill Where id = ?", new Object[]{billId}, new BillMapper());
    }

    @Override
    public List<Bill> getAllBills() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From bill", new BillMapper());
    }
}
