package com.epam.extlab2018.finalProject.option13.util.mappers;

import com.epam.extlab2018.finalProject.option13.dto.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.Request;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RequestMapper implements RowMapper<Request> {
    @Override
    public Request mapRow(ResultSet rs, int rowNums) throws SQLException {
        Request request = new Request();
        request.setRequestId(rs.getLong("id"));
        request.setSeatNumber(rs.getInt("seatNumber"));
        request.setApartmentClass(ApartmentClass.valueOf(rs.getString("apartmentClass").toUpperCase()));
        request.setStart(rs.getDate("start").toLocalDate());
        request.setEnd(rs.getDate("end").toLocalDate());
        request.setUserId(rs.getLong("user_id"));
        request.setHotelRoomId(rs.getLong("hotelRoom_id"));
        request.setBillId(rs.getLong("bill_id"));
        return request;
    }
}
