package com.epam.extlab2018.finalProject.option13.repository;

import com.epam.extlab2018.finalProject.option13.dto.ApartmentClass;
import com.epam.extlab2018.finalProject.option13.dto.HotelRoom;

import java.math.BigDecimal;
import java.util.List;

public interface HotelRoomRepository {
    long createHotelRoom(int seatNumber, ApartmentClass apartmentClass, BigDecimal dailyCost);

    void updateHotelRoom(long hotelRoomId, int seatNumber, ApartmentClass apartmentClass,
                         BigDecimal dailyCost);

    void deleteHotelRoom(long hotelRoomId);

    HotelRoom findHotelRoomById(long hotelRoomId);

    List<HotelRoom> getAllHotelRooms();
}
