package com.epam.extlab2018.finalProject.option13.dto;

public enum Role {
    CLIENT, ADMIN
}
